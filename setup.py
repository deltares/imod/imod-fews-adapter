from setuptools import setup

with open("README.rst") as f:
    long_description = f.read()

setup(
    name="imodfews",
    version="0.1.0",
    description="Adapter for running iMOD MODFLOW models from Delft-FEWS",
    long_description=long_description,
    url="https://gitlab.com/deltares/imod/imodfews",
    author="Martijn Visser",
    author_email="martijn.visser@deltares.nl",
    license="MIT",
    packages=["imodfews"],
    python_requires=">=3.6",
    install_requires=[
        "imod",
    ],
    extras_require={
        "dev": ["pytest"],
    },
    classifiers=[
        # https://pypi.python.org/pypi?%3Aaction=list_classifiers
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
    ],
    keywords="imod fews adapter",
)
