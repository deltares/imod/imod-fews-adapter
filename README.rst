This repository contains the adapter for running iMODFLOW models from Delft-FEWS.

Documentation can be found at the `Delft-FEWS wiki <https://publicwiki.deltares.nl/display/FEWSDOC/iMOD+Adapter>`__.
